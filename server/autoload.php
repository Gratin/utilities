<?php
spl_autoload_register('__autoloader');

function __autoloader($filename)
{
    preg_match_all('/([a-zA-Z0-9]+)/', $filename, $matches);
    $matches = array_shift($matches);

    $format = (function($file) {
        return $file.'.php';
    });
    
    try {
        $file = array_pop($matches);
        $fpath = implode('/', $matches);
        require_once $format($fpath.'/'.$file);
    } catch (\Exception $e) {
        throw new \FileNotFoundException('Class : '. $filename . ' not found');
    }
}