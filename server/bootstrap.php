<?php

$start = microtime(true);
$i = 0;
while ($i < 100000000) {
    echo '.';
    $i++;
}
$time = microtime(true) - $start;


$ns = microtime(true);
$i = 0;
while ($i < 100000000) {
    $handle = fopen('php://output', 'w', false, stream_context_get_default());
    fwrite($handle, '.');
    $i++;
}
$nt = microtime(true) - $ns;

echo $time . "<br>";
echo $nt . "<br>";

require_once './autoload.php';
use Volt\Http\Request;
use Volt\Http\Response;

$request = new Request();
$response = new Response();
$response->handle($request);
