<?php
declare(strict_types=1);
namespace Volt\Kernel;

class Wrapper
{
    public static function cli(): bool
    {
        return php_sapi_name() == "cli";
    }

    public static function error(string $s, bool $die = false)
    {
        self::write("\033[41m".$s."\033[0m");
        if ($die) {
            die;
        }
    }

    public static function warn(string $s)
    {
        return self::write("\033[1;31m".$s."\033[0m");
    }

    public static function info(string $s)
    {
        return self::write("\033[1;32m".$s."\033[0m");
    }

    public static function debug($value)
    {
        return self::write("\033[1;32m".$value."\033[0m");
    }

    public static function write(string $s)
    {
        echo $s;
        echo PHP_EOL;
    }
}