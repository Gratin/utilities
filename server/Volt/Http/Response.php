<?php
declare(strict_types=1);
namespace Volt\Http;

use Volt\Http\Request;
use Volt\Kernel\Wrapper;
use Volt\Http\Filters\HeadersFilter;

class Response
{
    public function __construct()
    {

    }

    public function handle(Request $request)
    {
        $this->send(['test'=>'Allo']);
    }

    public function send($content)
    {
        $handle = fopen('php://output', 'r+', false, stream_context_get_default());

        if (Wrapper::cli()) {
            fwrite($handle, "\033[1;31m".$content."\033[0m");
        } else {
            fwrite($handle, json_encode($content));
        }

        fclose($handle);
        exit;
    }
}