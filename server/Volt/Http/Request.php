<?php
declare(strict_types=1);
namespace Volt\Http;

use Volt\Http\Filters\HeadersFilter;

class Request
{
    const GET       = "GET";
    const POST      = "POST";
    const DELETE    = "DELETE";
    const PATCH     = "PATCH";
    const PUT       = "PUT";
    
    private $method;
    private $router;
    private $arguments;

    public function __construct(bool $routerEnabled = false)
    {
        $this->method = $_SERVER['REQUEST_METHOD'] ?? self::GET;
        $this->headers = $this->headers();
        $this->handle();
    }

    private function handle(): void
    {
        switch ($this->method) {
            case self::GET:
                $this->sanitize($_GET);
                break;
            case self::POST:
                $content = file_get_contents("php://input", false, stream_context_get_default(), 0, $_SERVER["CONTENT_LENGTH"]);
                $this->sanitize($content);
                break;
            case self::DELETE:
                break;
            case self::PUT:
                break;
            case self::PATCH:
                break;
            default:
                throw new \MethodNotAllowedException('Method not allowed');
        }
    }

    private function sanitize(array $data): void
    {
        $this->arguments = $data;
    }

    private function headers(): void
    {
        $this->headers = getallheaders();
    }
}
