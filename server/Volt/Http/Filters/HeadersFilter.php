<?php
namespace Volt\Http\Filters;

/**
 *      stream_filter_register('headersfilters', 'Volt\\Http\\Filters\\HeadersFilter');
 *      stream_filter_append($handle, 'headersfilters'); 
 * 
 */
class HeadersFilter extends \php_user_filter
{
    function filter($in, $out, &$consumed, $closing)
    {
        while ($bucket = stream_bucket_make_writeable($in)) {
            //   $bucket->data = strtoupper($bucket->data);
            $consumed += $bucket->datalen;
            stream_bucket_append($out, $bucket);
        }
        return PSFS_PASS_ON;
    }
}