<?php
/**
 * Author: Luc-Olivier Noel 
 */
$url = $argv[1] ?? '';

$errors = [];
$visited = [];
$status = [];

class Crawler
{
    const NOT_FOUND         = 404;
    const SUCCESS_CODES     = [200, 301, 302];
    const REDIRECT_CODES    = [301 => 'permanent', 302 => 'temporary'];
    const REDIRECTS_HEADERS = ['URL', 'TO'];
    const ERRORS_HEADERS    = ['URL', 'Erreurs', 'FROM'];
    const VISITED_HEADERS   = ['URL', 'Reponse HTTP'];
    const TIMES_HEADERS     = ['URL', 'Temps de chargement'];

    private $visited        = [];
    private $errors         = [];
    private $tree           = [];
    private $times          = [];
    private $redirects      = [];
    
    private $configs        = ['depth'=>20];
    private $depth          = null;


    public function __construct(array $configs = [])
    {
        $this->reset();
        $this->configs = array_merge($this->configs, $configs);
        $this->depth = $configs['depth'];
    }

    public function boot(string $url)
    {
        $this->info((new \DateTime())->format('Y-m-d H:i:s'));

        $data = parse_url($url);
        $this->scheme = $data['scheme']."://";
        $this->domain = $data['host'];

        DomainHelper::getData($url);

        $this->crawl($url, $url);
    }

    private function visit(string $url, string $parent = null)
    {
        $xml = new DOMDocument();

        libxml_use_internal_errors(true);
        // Prevents from display HTML errors
        $xml->loadHTMLFile($url);
        libxml_clear_errors();
        foreach($xml->getElementsByTagName('a') as $link) {


            $cUrl = $link->getAttribute('href');
            $class = $link->getAttribute('class');
            $cUrl = $this->patchUrl($cUrl);

            if (!$this->isInternal($cUrl)) {
                continue;
            }

            $this->isSecure($link->getAttribute('href'));

            if (in_array($cUrl, $this->visited)) {
                continue;
            } else {
                $this->visited[] = $cUrl;
            }

            if (count($this->visited) >= $this->depth) {
                $this->end();
            }

            $this->info("VISITING ::: ". $cUrl);
            $this->crawl($cUrl, $url . " - ". $class);
        }
    }
    
    private function isInternal($url)
    {
        $urlData = parse_url($url);
        if (empty($urlData) || !array_key_exists('host', $urlData)) { return false; }
        return (bool)($urlData['host'] === $this->domain);
    }

    private function crawl(string $url, ?string $parent = null)
    {
        $status = $this->getStatus($url, $parent);

        if ($status) {
            $this->visit($url, $parent);
        }
    }

    private function getStatus(string $url, ?string $parent = null)
    {
        $returns = $this->createCurl($url);

        if ($returns['status'] === self::NOT_FOUND) {
            $this->addError($url, self::NOT_FOUND, $parent);
        }

        if ($this->hasTimer()) {
            $this->addTimer($url, $returns['time']);
        }

        if ($this->isRedirect($url, $returns['status'])) {
            $this->addRedirect($url, $returns);
        }

        if (in_array($returns['status'], self::SUCCESS_CODES)) {
            return true;
        }

        return false;
    }

    private function addRedirect(string $url, array $infos)
    {
        $this->redirects[$url] = ['status'=>$infos['status'], 'to'=>$infos['to']];
    }

    private function isRedirect(string $url, int $status): bool
    {
        if (in_array($status, self::REDIRECT_CODES)) {
            return true;
        }

        return false;
    }

    private function addError(string $url, $error, ?string $parent = null)
    {
        if (!array_key_exists($url, $this->errors)) {
            $this->errors[$url] = [];
        }

        $this->errors[$url] = [$error, $parent];
    }

    private function addTimer(string $url, $time)
    {
        if (!array_key_exists($url, $this->times)) {
            $this->times[$url] = [];
        }

        $this->times[$url][] = $time;
    }

    private function isSecure($url)
    {
        return (stripos($url, "https") !== false);
    }

    private function hasTimer()
    {
        return array_key_exists('timer', $this->configs);
    }

    public function patchUrl($url)
    {
        if (stripos($url, 'http') === false) {
            return $this->scheme . $this->domain . $url;
        }
    }
    private function createCurl($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        $infos = curl_getinfo($ch);
        curl_close($ch);

        $time = [];
        if ($this->hasTimer()) {
            $time = ['time'=>$infos['total_time']];
        }

        return array_merge($time, ['status'=>$infos['http_code'], 'to'=>$infos['redirect_url']]);
    }

    private function reset()
    {
        $this->visited      = [];
        $this->errors       = [];
        $this->tree         = [];
        $this->redirects    = [];
        $this->times        = [];
    }

    public function report()
    {
        $this->render('times');
        $this->render('errors');
        $this->render('visited');
        $this->render('redirects');
    }

    private function render(string $key)
    {
        $data       = $this->{$key} ?? [];
        $handle     = fopen($key.'.csv', 'w+');
        $headers    = constant(__CLASS__."::".strtoupper($key).'_HEADERS');
        ftruncate($handle, 0);
        fputcsv($handle, $headers);

        foreach ($data as $k => $row) {
            if (!is_array($row)) {
                $row = [$row];
            }
            fputcsv($handle, array_merge([$k], $row));
        }

        fclose($handle);
    }

    public function end()
    {
        $this->report();
        $this->info((new \DateTime())->format('Y-m-d H:i:s'));
        $this->info('Done');
        exit;
    }

    private function error(string $s, bool $die = false)
    {
        $this->write("\033[41m".$s."\033[0m");
        if ($die) {
            die;
        }
    }

    private function warn(string $s)
    {
        return $this->write("\033[1;31m".$s."\033[0m");
    }

    private function info(string $s)
    {
        return $this->write("\033[1;32m".$s."\033[0m");
    }

    public function write(string $s)
    {
        echo $s;
        echo PHP_EOL;
    }
}

try {
    $url = $argv[1];
} catch (\Exception $e) {
    throw new \Exception('No url provided.');
    exit;
}

$c = new Crawler(
    [
        'timer' =>  true,
        'secure'=>  true,
        'depth' =>  20000
    ]
);


$c->boot($url);
$c->report();

// $c->write('Done');
exit;