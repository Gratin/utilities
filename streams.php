<?php

$request = new Request();
$response = new Response();
$response->send('Allo');

class Request
{
    const GET = "GET";
    const POST = "POST";
    const DELETE = "DELETE";
    const PATCH = "PATCH";
    const PUT = "PUT";
    
    private $method;
    private $arguments;
    private $router;

    public function __construct($routerEnabled = false)
    {
        $this->method = $_SERVER['REQUEST_METHOD'] ?? self::GET;

        $this->handle();
    }

    private function handle()
    {
        switch ($this->method) {
            case self::GET:
                $this->sanitize($_GET);
                break;
            case self::POST:
                $content = file_get_contents("php://input", false, stream_context_get_default(), 0, $_SERVER["CONTENT_LENGTH"]);
                $this->sanitize($content);
                break;
            case self::DELETE:
                break;
            case self::PUT:
                break;
            case self::PATCH:
                break;
            default:
                throw new \MethodNotAllowedException('Method not allowed');
        }
    }

    private function sanitize(array $data)
    {
        $this->arguments = $data;
    }
}

class Response
{
    public function __construct()
    {

    }

    public function send($content)
    {
        $handle = fopen('php://output', 'r+');
    
        if (Wrapper::name() === "cli") {
            fwrite($handle, "\033[1;31m".$content."\033[0m");
        } else {
            fwrite($handle, $content);
        }

        fclose($handle);
        exit;
    }
}