<?php
namespace App\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

class capFunctionNAMEFunction extends Twig_Extension
{
    /**
      * Functions
      * @return array
      */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('functionNAME', [$this, 'functionNAME']),
        ];
    }
    
    /**
     * 
     * @return
     */
    public function functionNAME()
    {

    }
}
