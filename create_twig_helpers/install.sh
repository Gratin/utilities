#!/bin/bash
FUNCTION=$1
BASEDIR=$(pwd)
# echo $BASEDIR/app/Twig/$1.php
FUNC="$(tr '[:lower:]' '[:upper:]' <<< ${FUNCTION:0:1})${FUNCTION:1}"
FILENAME="$BASEDIR/app/Twig/${FUNC}Function.php"
cp /usr/local/opt/twiginstall/base.php $FILENAME
file_contents=$(<$FILENAME)
echo "${file_contents//functionNAME/$FUNCTION}" > $FILENAME
file_contents=$(<$FILENAME)
echo "${file_contents//capFunctionNAME/$FUNC}" > $FILENAME

echo -e "\033[0;102mPlease add 'App\\Twig\\${FUNC}Function' into config/twigbridge.php in the extensions.enabled array.\033[0m"
