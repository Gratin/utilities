<?php
// Looper a travers les fichiers d'un dossier,
class MyGlobIterator extends \GlobIterator {}

function getGlobIterator($path)
{
    $iterator = new MyGlobIterator($path);
    return $iterator;
}

// Looper a travers les fichiers d'un dossier,
function inspectFolder($path)
{
    echo ":::: INSPECT FOLDER ::::", PHP_EOL;
    $iterator = getGlobIterator($path);
    foreach ($iterator as $key => $value) {
        $file = $iterator->current();
        print_r($file);
    }

    echo " :::: END INSPECT FOLDER ::::", PHP_EOL;
}

inspectFolder('./*');

// Bla bla bla, pourquoi?
$array = ['a', 'g', 'd', 'f', 'e'];
$keyedArray = ['ee'=>'a', 'qq'=>'g', 'gg'=>'d', 'ff'=>'f', 'cc'=>'e'];
$mixedArray = [1, 'a', 'r', 53, 'y', 'bv', 122];

$arrayIterator = new \ArrayIterator($array);
// echo $arrayIterator->current(), PHP_EOL; // a

foreach ($arrayIterator as $key => $value) {
    echo $value, PHP_EOL;
}

echo "AT THE END ", $arrayIterator->current(), PHP_EOL;  // null
$arrayIterator->rewind(); // retourne au debut du iterator
echo "AFTER REWING ", $arrayIterator->current(), PHP_EOL;  // a

// But what about
class AlphaSortedIterator extends \ArrayIterator
{
    public function __construct($array, $keyed = false, $flags = ArrayIterator::STD_PROP_LIST)
    {
        if ($keyed) {
            ksort($array);
        } else {
            asort($array);
        }
        parent::__construct($array, $flags);
    }

    public function append($value)
    {
        parent::append($value);
        $this->asort();
    }
}

$sorted = new \AlphaSortedIterator($array);
print_r($sorted->getArrayCopy());
echo $sorted[0], PHP_EOL;
// With keys
$ksort = new \AlphaSortedIterator($keyedArray);
echo $ksort->current(), PHP_EOL;

// BUT WAIT
$arrayIterator->asort();
print_r($arrayIterator->getArrayCopy());
$sorted->append('a');
print_r($sorted->getArrayCopy());

// Cache
// To use as objects
$cachingIterator = new \CachingIterator($arrayIterator, CachingIterator::TOSTRING_USE_CURRENT);
// Caching Iterator est tjrs 1 en arriere de son iterator interne
foreach ($cachingIterator as $key => $value) {
    echo "Current :: ", $cachingIterator->current(), PHP_EOL; // ou $value
    echo "Next :: " . $cachingIterator->getInnerIterator()->current(), PHP_EOL; // Da next value. 
}


// InfiniteIterator & LimitIterator
// Infinite permet de looper sans devoir "rewind"
// LimitIterator defini les "boundaries" du loop
// Meilleur exemple serait un calendrier
$infinite = new \InfiniteIterator($arrayIterator);
foreach (new \LimitIterator($infinite, 0, 10) as $key => $value) {
    echo $value, PHP_EOL;
}

/**
 * On ajoute des iterator dans un AppendIterator pour looper a travers les 2 iterators
 */ 
$parent = getGlobIterator("../*");
$self = getGlobIterator("./*");

$aggregate = new \AppendIterator();
$aggregate->append($parent);
$aggregate->append($self);

foreach ($aggregate as $key => $value) {
    print_r($key);
    echo PHP_EOL;
    print_r($value);
    echo PHP_EOL;
    print_r($aggregate->getInnerIterator());
    echo PHP_EOL;
    print_r((string)$aggregate->getIteratorIndex());
    echo PHP_EOL;
}


